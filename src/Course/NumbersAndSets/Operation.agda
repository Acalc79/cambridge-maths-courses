{-# OPTIONS --exact-split #-}
module Course.NumbersAndSets.Operation where

open import Course.NumbersAndSets.Operation.Base public
open import Course.NumbersAndSets.Operation.Abstract public
open import Course.NumbersAndSets.Operation.Definitions public
