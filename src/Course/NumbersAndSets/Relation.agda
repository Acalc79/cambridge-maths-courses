{-# OPTIONS --exact-split #-}
module Course.NumbersAndSets.Relation where

open import Course.NumbersAndSets.Base

module RelVars where
  variable R' P P' Q : set
open SetVars
open RelVars using (R')

_is-rel-on_ : (R A : set) → Set
R is-rel-on A = R ∶ A ⇸ A

module Notation R where
  _~_ _R_ : (a b : set) → Set
  a R b = ⟨ a ، b ⟩ ∈ R
  a ~ b = ⟨ a ، b ⟩ ∈ R

-- Definition (Partition of set)
_is-partition-of_ : (F X : set) → Set
F is-partition-of X = (⋀ A ∈ F , A ⊆ X) ∧ (⋀ x ∈ X , ∃! λ A → A ∈ F ∧ x ∈ A)

module RelDefs R' A where
  open Notation R' public

  reflexive symmetric symmetric' transitive : Set
  reflexive = ⋀ a ∈ A , a ~ a
  symmetric = ∀{a b} → a R b → b R a
  transitive = ∀{a b c} → a R b → b R c → a R c

  -- from the course, more cumbersome
  symmetric' = ∀{a b} → a R b ⇔ b R a

  symmetric⇔symmetric' : symmetric ⇔ symmetric'
  symmetric⇔symmetric' = mk⇔
    (λ aRb→bRa {a}{b} → mk⇔ (aRb→bRa {a}{b})(aRb→bRa {b}{a}))
    (λ aRb⇔bRa {a}{b} → from (aRb⇔bRa {a}{b}))

  equivalence : Set
  equivalence = reflexive ∧ symmetric ∧ transitive

  -- Definition (Equivalence class)
  abstract
    ⟦_⟧ : (x : set) → set
    ⟦ x ⟧ = ｛ a ∈ A ∣ x ~ a ｝

    ∈⟦⟧ : ∀{a} → a ∈ ⟦ x ⟧ ⇔ a ∈ A ∧ x ~ a
    ∈⟦⟧ {x} = ∈｛ a ∈ A ∣ x ~ a ｝

  module _ (equiv : equivalence) where
    ~-refl = proj₁ equiv
    ~-sym = proj₁ (proj₂ equiv)
    ~-trans = proj₂ (proj₂ equiv)

    abstract
      ⟦⟧≡ : ⋀ y ∈ A , (⟦ x ⟧ ≡ ⟦ y ⟧ ⇔ x ~ y)
      ⟦⟧≡ {x}{y} y∈A = mk⇔
        (λ [x]≡[y] → to ∈⟦⟧ (y∈A , ~-refl y∈A) ∶ y ∈ ⟦ y ⟧
                   |> subst (y ∈_)(sym [x]≡[y]) ∶ y ∈ ⟦ x ⟧
                   |> proj₂ F.∘ from ∈⟦⟧ ∶ x ~ y)
        λ x~y → antisym-⊆
          (λ {a} a∈[x] → case from ∈⟦⟧ a∈[x] of λ { (a∈A , x~a) →
            to ∈⟦⟧ $ a∈A , ~-trans (~-sym x~y) x~a})
          (λ {a} a∈[y] → case from ∈⟦⟧ a∈[y] of λ { (a∈A , y~a) →
            to ∈⟦⟧ $ a∈A , ~-trans x~y y~a})

      ⟦⟧-fun : fun-reify ⟦_⟧ A ∶ A ⟶ ｛ ⟦ x ⟧ ∣ x ∈ A ｝
      ⟦⟧-fun = ∶⟶-cong-⊆ refl
        (λ {X} X∈ran⟦⟧ → case from ∈ran X∈ran⟦⟧ of λ
          { (a , aX∈f) →
          let f = fun-reify ⟦_⟧ A
              a∈A = dom⊆ (fun→rel fun-fun-reify) $ to ∈dom $ X , aX∈f
              open ≡-Reasoning
              [a]≡X : ⟦ a ⟧ ≡ X
              [a]≡X = begin ⟦ a ⟧ ≡⟨ sym $ valid-fun-reify a∈A ⟩
                            f [ a ] ≡⟨ proj₂ $ to (fun-fun-reify [ a ]≡) aX∈f ⟩
                            X ∎
          in to (∈fun-rep ⟦_⟧) $ a , a∈A , [a]≡X})
        fun-fun-reify

      -- Theorem
      quotient-set-partition : ｛ ⟦ x ⟧ ∣ x ∈ A ｝ is-partition-of A
      quotient-set-partition =
        (λ {X} X∈Q {a} a∈X → case from ∈Q X∈Q of λ
          { (x , x∈A , refl) → proj₁ $ from ∈⟦⟧ a∈X}) ,
        λ {x} x∈A →
          ⟦ x ⟧ ,
          (to ∈Q (x , x∈A , refl) , to ∈⟦⟧ (x∈A , ~-refl x∈A)) ,
          λ { {X} (X∈Q , x∈X) → case from ∈Q X∈Q of λ
            { (y , y∈A , refl) → to (⟦⟧≡ y∈A) $ ~-sym $ proj₂ $ from ∈⟦⟧ x∈X}}
        where Q = ｛ ⟦ x ⟧ ∣ x ∈ A ｝
              ∈Q = ∈fun-rep ⟦_⟧

open import Function.Equivalence.Reasoning
open import Logic.Properties

module _ (R-rel : R' is-rel-on A) where
  open RelDefs R' A

  a~b→a∈A : a ~ b → a ∈ A
  a~b→b∈A : a ~ b → b ∈ A
  a~b→a∈A a~b = proj₁ $ from ⟨,⟩∈× $ R-rel a~b
  a~b→b∈A a~b = proj₂ $ from ⟨,⟩∈× $ R-rel a~b

  ∈⟦_⟧ : ∀{x a} → a ∈ ⟦ x ⟧ ⇔ x ~ a
  ∈⟦_⟧ {x}{a} = begin
    a ∈ ⟦ x ⟧ ⇔⟨ ∈⟦⟧ ⟩
    (a ∈ A ∧ x ~ a) ⇔⟨ [B→A]→A∧B⇔B a~b→b∈A ⟩
    (x ~ a) ∎

module _ {F}(F-part : F is-partition-of A) where
  private get-set = proj₂ F-part

  rel-from-part : set
  rel-from-part = reify A A λ x y → ⋁ X ∈ F , x ∈ X ∧ y ∈ X

  open RelDefs rel-from-part A

  rel-from-part-equiv : equivalence
  rel-from-part-equiv =
    (λ {x} x∈A → case get-set x∈A of λ { (X , (X∈F , x∈X) , _) →
      to ⟨,⟩∈reify $ x∈A , x∈A , X , X∈F , x∈X , x∈X}) ,
    (λ {a}{b} a~b → case from ⟨,⟩∈reify a~b of λ
      { (a∈A , b∈A , X , X∈F , a∈X , b∈X) → to ⟨,⟩∈reify $
         b∈A , a∈A , X , X∈F , b∈X , a∈X}) ,
    λ {a}{b}{c} a~b b~c → case from ⟨,⟩∈reify a~b of λ
      { (a∈A , b∈A , X , X∈F , a∈X , b∈X) → case from ⟨,⟩∈reify b~c of λ
      { (_ , c∈A , Y , Y∈F , b∈Y , c∈Y) → case get-set b∈A of λ
      { (Z , (Z∈F , _) , !Z) → case !Z (X∈F , b∈X) , !Z (Y∈F , b∈Y) of λ
      { (refl , refl) → to ⟨,⟩∈reify $ a∈A , c∈A , Z , Z∈F , a∈X , c∈Y }}}}
