{-# OPTIONS --exact-split #-}
module Course.NumbersAndSets.Base where

open import ZF.Foundation hiding (_[_∈dom]; _∘_; id; module RecDef) public
open import ZF.Function hiding (restrict) public
open import ZF.Relation.Base public

module F where
  open ZF.Foundation public
module Fun where
  open ZF.Function public

module SetVars where
  variable A B C X Y Z x y z a b c d : set
open SetVars

-- Definition (symmetric difference)
infixr 22 _Δ_
_Δ_ : (A B : set) → set
A Δ B = (A - B) ∪ (B - A)

open import Function.Equivalence.Reasoning

infixr 1 _xor_
_xor_ : (P Q : Set) → Set
P xor Q = P ∧ ¬ Q ∨ Q ∧ ¬ P

open import Logic.Properties

∈Δ : (x ∈ A Δ B) ⇔ (x ∈ A xor x ∈ B)
∈Δ {x} {A} {B} = begin
  x ∈ A Δ B ≡⟨⟩
  x ∈ (A - B) ∪ (B - A) ⇔⟨ ∈∪ ⟩
  (x ∈ A - B ∨ x ∈ B - A) ⇔⟨ ∨-cong-⇔ ∈- ∈- ⟩
  (x ∈ A ∧ x ∉ B ∨ x ∈ B ∧ x ∉ A) ≡⟨⟩
  (x ∈ A xor x ∈ B) ∎

-- Definition (Permutation)
_is-perm-of_ : (f A : set) → Set
f is-perm-of A = f ∶ A ⟶ A ∧ᵈ bijection
