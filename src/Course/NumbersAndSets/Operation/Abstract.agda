{-# OPTIONS --exact-split #-}
module Course.NumbersAndSets.Operation.Abstract where

open import Course.NumbersAndSets.Base

open import Level hiding (suc)
open import Data.Nat as Nat hiding (_⊔_)
open import Data.Fin as Fin using (Fin)
open import Data.Fin.Patterns
open import Data.Vec as Vec hiding ([_])

private variable n m k : ℕ

data TermAST (v : Vec ℕ n)(m : ℕ) : Set where
  const : set → TermAST v m
  var : Fin m → TermAST v m
  op-node : (k : Fin n) → Vec (TermAST v m)(lookup v k) → TermAST v m

infix 21 _==_ _closed
data Propositional (v : Vec ℕ n)(m : ℕ) : Set where
  _==_ : (t₁ t₂ : TermAST v m) → Propositional v m
  _closed : (t : TermAST v m) → Propositional v m

data PropertyAST (v : Vec ℕ n) : Set where
  for_vars:_ : (m : ℕ)(P[m] : Propositional v m) → PropertyAST v

open import Data.Unit

module _ {ℓ₀ ℓ₁}(arg : Set ℓ₀)(ret : Set (ℓ₀ ⊔ ℓ₁)) where
  n-arg : (m : ℕ) → Set (ℓ₀ ⊔ ℓ₁)
  n-arg zero = ret
  n-arg (suc m) = arg → n-arg m

  const-n-arg : (C : ret)(m : ℕ) → n-arg m
  const-n-arg C 0 = C
  const-n-arg C (suc m) = λ _ → const-n-arg C m

module Private {ℓ₀ ℓ₁}{X : Set ℓ₀}{Y : Set ℓ₁} where
  fmap' : (n : ℕ)(f : X → Y) → n-arg set X n → n-arg set Y n
  ap' : (n : ℕ) → n-arg set (X → Y) n → n-arg set X n → n-arg set Y n

  fmap' 0 f x = f x
  fmap' (suc n) f arg→x = fmap' n f F.∘ arg→x

  ap' 0 f x = f x
  ap' (suc n) arg→f arg→x arg = ap' n (arg→f arg) (arg→x arg)

module Instances (n : ℕ){ℓ₀ ℓ₁}{X : Set ℓ₀}{Y : Set ℓ₁} where
  open Private

  infixl 18 _<$>_
  infixl 17 _<*>_
  fmap _<$>_ : (f : X → Y) → n-arg set X n → n-arg set Y n
  fmap = fmap' n
  _<$>_ = fmap

  ap _<*>_ : n-arg set (X → Y) n → n-arg set X n → n-arg set Y n
  ap = ap' n
  _<*>_ = ap

_$$_ : ∀{ℓ₀ ℓ₁}{X : Set ℓ₀}{Y : Set (ℓ₀ ⊔ ℓ₁)} →
  n-arg {ℓ₁ = ℓ₁} X Y n → Vec X n → Y
y $$ [] = y
x→rest $$ (x ∷ xs) = x→rest x $$ xs

OpInterpretation : (v : Vec ℕ n) → Set
OpInterpretation {n} v = (k : Fin n) → n-arg set set (lookup v k)

InterpretTerm : (v : Vec ℕ n) → TermAST v m → OpInterpretation v → n-arg set set m

map-interpret : (v : Vec ℕ n) →
  Vec (TermAST v m) k → OpInterpretation v → n-arg set (Vec set k) m
map-interpret v [] I = const-n-arg set (Vec set 0) [] _
map-interpret {m = m} v (t₀ ∷ ts) I =
  _∷_ <$> InterpretTerm v t₀ I <*> map-interpret v ts I
  where open Instances m

InterpretTerm {m = m} v (op-node k ts) I =
  _$$_ <$> const-n-arg set _ (I k) m <*> map-interpret v ts I
  where open Instances m
InterpretTerm {m = m} _ (const a) I = const-n-arg set set a m
InterpretTerm {m = suc m} _ (var 0F) I = λ x → const-n-arg set set x m
InterpretTerm {m = suc (suc m)} v (var (Fin.suc x)) I =
  λ _ → InterpretTerm v (var x) I

multiple-⋀ : (m : ℕ)(A : set) → n-arg set Set m → Set
multiple-⋀ zero A ϕ = ϕ
multiple-⋀ (suc m) A ϕ = ⋀ x ∈ A , multiple-⋀ m A (ϕ x)

InterpretPropositional :
  (v : Vec ℕ n)(A : set)(I : OpInterpretation v)
  (P[m] : Propositional v m) → n-arg set Set m
InterpretPropositional {m = m} v A I (t₁ == t₂) =
  _≡_ <$> InterpretTerm v t₁ I  <*> InterpretTerm v t₂ I
  where open Instances m
InterpretPropositional {m = m} v A I (t closed) = _∈ A <$> InterpretTerm v t I
  where open Instances m

PropertyFromAST : (v : Vec ℕ n)(A : set)(I : OpInterpretation v)
  (P : PropertyAST v) → Set
PropertyFromAST v A I (for m vars: P[m]) =
  multiple-⋀ m A $ InterpretPropositional v A I P[m]

OneOpPropertyFromAST : ∀ A op → PropertyAST Vec.[ 2 ] → Set
OneOpPropertyFromAST A op ast = PropertyFromAST Vec.[ 2 ] A (λ {0F → op}) ast

TwoOpPropertyFromAST : ∀ A op+ op* → PropertyAST (2 ∷ 2 ∷ []) → Set
TwoOpPropertyFromAST A op+ op* ast = PropertyFromAST (2 ∷ 2 ∷ []) A
  (λ { 0F → op+ ; 1F → op* }) ast

op-dereify : (op : set)(arity : ℕ) → n-arg set set arity

get-args : (arity : ℕ) → n-arg set set (suc arity)

op-dereify op 0 = op
op-dereify op (suc arity) = op [_] <$> get-args arity
  where open Instances (suc arity)

get-args 0 x = x
get-args (suc arity) x = ⟨ x ،_⟩ <$> get-args arity
  where open Instances (suc arity)

OneSetOpPropertyFromAST : ∀ A op → PropertyAST Vec.[ 2 ] → Set
OneSetOpPropertyFromAST A op = OneOpPropertyFromAST A (op-dereify op 2)

TwoSetOpPropertyFromAST : ∀ A op+ op* → PropertyAST (2 ∷ 2 ∷ []) → Set
TwoSetOpPropertyFromAST A op+ op* =
  TwoOpPropertyFromAST A (op-dereify op+ 2)(op-dereify op* 2)

module Test where
    v = Vec.[ 2 ]
    t = λ {n} → TermAST v n

    infixl 22 _∙_
    _∙_ : t {n} → t → t
    x ∙ y = op-node Fin.zero (x ∷ y ∷ [])
    x y z : t {3}
    x = var 0F
    y = var 1F
    z = var 2F

    assocAST : PropertyAST (Vec.[ 2 ])
    assocAST = for 3 vars: x ∙ (y ∙ z) == x ∙ y ∙ z

    assocProp : ∀ A op → Set
    assocProp A op = OneOpPropertyFromAST A op assocAST

    assocSetProp : ∀ A op → Set
    assocSetProp A op = OneSetOpPropertyFromAST A op assocAST
