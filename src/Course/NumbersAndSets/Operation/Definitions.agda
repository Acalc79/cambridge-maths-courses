{-# OPTIONS --exact-split #-}
open import Course.NumbersAndSets.Operation.Base

module Course.NumbersAndSets.Operation.Definitions where

open import Course.NumbersAndSets.Base
open import Course.NumbersAndSets.Operation.Abstract

open import Data.Vec as Vec
open import Data.Nat as Nat hiding (_+_; _*_)
open import Data.Fin.Patterns
private variable n m : ℕ

private
  infixl 22 _+_
  _+_ : ∀{v' : Vec ℕ n} → let v = 2 ∷ v' in
    TermAST v m → TermAST v m → TermAST v m
  x + y = op-node 0F (x ∷ y ∷ [])
  x : ∀{v : Vec ℕ n} → TermAST v (1 Nat.+ m)
  y : ∀{v : Vec ℕ n} → TermAST v (2 Nat.+ m)
  z : ∀{v : Vec ℕ n} → TermAST v (3 Nat.+ m)
  x = var 0F
  y = var 1F
  z = var 2F

module SingleOp A op+ where
  private
    fromAST = OneOpPropertyFromAST A op+

  Closed Associative Commutative : Set
  Closed = fromAST $ for 2 vars: x + y closed
  Associative = fromAST $ for 3 vars: x + (y + z) == x + y + z
  Commutative = fromAST $ for 2 vars: x + y == y + x

  LeftIdentity RightIdentity : (e : set) → Set
  LeftIdentity e = fromAST $ for 1 vars: const e + x == x
  RightIdentity e = fromAST $ for 1 vars: x + const e == x

  LeftZero RightZero : (z : set) → Set
  LeftZero z = fromAST $ for 1 vars: const z + x == const z
  RightZero z = fromAST $ for 1 vars: x + const z == const z
  

module TwoOp A op* op+ where
  private
    fromAST = TwoOpPropertyFromAST A op+ op*
    infixl 23 _*_
    _*_ : ∀{x}{v' : Vec ℕ n} → let v = x ∷ 2 ∷ v' in
      TermAST v m → TermAST v m → TermAST v m
    x * y = op-node 1F (x ∷ y ∷ [])

  _DistributesOverˡ_ _DistributesOverʳ_ : Set
  _DistributesOverˡ_ = fromAST $ for 3 vars: x * (y + z) == x * y + x * z
  _DistributesOverʳ_ = fromAST $ for 3 vars: (y + z) * x == y * x + z * x

  -- open import Algebra.Definitions
