{-# OPTIONS --exact-split #-}
module Course.NumbersAndSets.Operation.Base where

open import Course.NumbersAndSets.Base
import Course.NumbersAndSets.Relation as Rel

_is-op-on_ : (op A : set) → Set
op is-op-on A = op ∶ A × A ⟶ A

module Notation op where
  [_∙_]=_ : (x y z : set) → Set
  [ x ∙ y ]= z = op[ ⟨ x ، y ⟩ ]= z
    where open Rel.Notation op renaming (_~_ to op[_]=_)

  infixl 25 _∙_
  _∙_ : (x y : set) → set
  x ∙ y = op [ ⟨ x ، y ⟩ ]

[∙]≡ : ∀{A op}
  (op-is-op : op is-op-on A) →
  let open Notation op in
  ∀{x y z} → [ x ∙ y ]= z ⇔ (x ∈ A ∧ y ∈ A ∧ x ∙ y ≡ z)
[∙]≡ fun {x}{y} = mk⇔
  (λ xyz∈op → case to (fun [ ⟨ x ، y ⟩ ]≡) xyz∈op of λ
    { (xy∈A² , refl) → case from ⟨,⟩∈× xy∈A² of λ
    { (x∈A , y∈A) → x∈A , y∈A , refl}})
  λ { (x∈A , y∈A , refl) →
      from (fun [ ⟨ x ، y ⟩ ]≡) $ to ⟨,⟩∈× (x∈A , y∈A) , refl}

op-reify : (A : set)(op : (x y : set) → set) → set
op-reify A op = reify (A × A) A λ x y → ∃₂ λ a b → ⟨ a ، b ⟩ ≡ x ∧ op a b ≡ y

op-reify-op : ∀{A op}(closed : ⋀ a ∈ A , ⋀ b ∈ A , op a b ∈ A)
  → ----------------------------------------------------------------------
  op-reify A op is-op-on A
op-reify-op {A}{op} closed = ∶⇀-⁻¹surj→∶⟶ op-reify-pfun λ {x} x∈A² →
  case from ∈× x∈A² of λ { (a , a∈A , b , b∈A , refl) →
  op a b , to ⟨,⟩∈⁻¹ (to ⟨,⟩∈reify $
  to ⟨,⟩∈× (a∈A , b∈A) , closed a∈A b∈A , a , b , refl , refl)}
  where
  op-reify-pfun : op-reify A op ∶ A × A ⇀ A
  op-reify-pfun = rel-⁻¹inj→∶⇀ reify-rel λ {x}{x'}{y} xy∈op x'y∈op →
    case from ⟨,⟩∈reify (from ⟨,⟩∈⁻¹ xy∈op) ,
         from ⟨,⟩∈reify (from ⟨,⟩∈⁻¹ x'y∈op) of λ
    { ((_ , _ , a , b , refl , refl) ,
       (_ , _ , a' , b' , a'b'≡ab , refl)) → case from ⟨,⟩≡ a'b'≡ab of λ
    { (refl , refl) → refl}} 
