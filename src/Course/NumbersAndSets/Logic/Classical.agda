{-# OPTIONS --without-K --exact-split --safe #-}
open import Level
open import Axiom.ExcludedMiddle

module Course.NumbersAndSets.Logic.Classical (em : ExcludedMiddle 0ℓ) where

open import Relation.Nullary using (Dec; yes; no) public

is? : ∀ P → Dec P
is? P = em {P = P}

open import Course.NumbersAndSets.Logic
open LogicVars

¬∀⇔∃¬ : ¬ (∀ x → P[ x ]) ⇔ ∃ λ x → ¬ P[ x ]
¬∀⇔∃¬ =
  [⇒]: (λ ¬∀ → {!!})
  [⇐]: {!!}
