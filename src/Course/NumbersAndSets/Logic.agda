{-# OPTIONS --without-K --exact-split --safe #-}
module Course.NumbersAndSets.Logic where

open import Level using (Level; 0ℓ; _⊔_; suc) public
open import Data.Empty
import Data.Product as Product
import Data.Sum as Sum
open Product using (_×_; Σ-syntax)
open Product public using (_,_; proj₁; proj₂)
open Sum using (_⊎_)

variable ℓ ℓ₀ ℓ₁ ℓ₂ l l₀ l₁ l₂ : Level

-- 1ℓ = ℓ-suc 0ℓ
-- 2ℓ = ℓ-suc 1ℓ

Statement : Set₁
Statement = Set

infixr 8 ¬_
infixr 7 _∧_
infixr 6 _∨_ _xor_
infixr 5 _⇒_
infix 4 _⇔_

_∧_ _∨_ _⇒_ _xor_ : (P Q : Statement) → Statement
¬_ : (P : Statement) → Statement
False True : Statement

False = ⊥
_∧_ = _×_
_∨_ = _⊎_
P ⇒ Q = P → Q
P xor Q = (P ∧ ¬ Q) ∨ (Q ∧ ¬ P)

record _⇔_ (P Q : Statement) : Statement where
  constructor [⇒]:_[⇐]:_
  field
    [⇒] : P ⇒ Q
    [⇐] : Q ⇒ P

¬ P  = P ⇒ False
True = False ⇒ False

infix 2 ∃-syntax ∀-syntax

∃-syntax ∀-syntax :
  (X : Set)(P[_] : (x : X) → Statement) → Statement
∃-syntax = Σ-syntax
∀-syntax _ P[_] = ∀ x → P[ x ]

syntax ∃-syntax X (λ x → P[x]) = ∃[ x ∈ X ] P[x]
syntax ∀-syntax X (λ x → P[x]) = ∀[ x ∈ X ] P[x]

module LogicVars where
  private variable X : Set ℓ

  variable P Q R P' Q' R' : Statement
           P[_] Q[_] R[_] P'[_] Q'[_] R'[_] : X → Statement
