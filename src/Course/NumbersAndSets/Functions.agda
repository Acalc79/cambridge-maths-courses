{-# OPTIONS --exact-split #-}
module Course.NumbersAndSets.Functions where

open import Course.NumbersAndSets.Base

module FunctionVars where
  variable f g h f' g' : set
open SetVars
open FunctionVars

-- Definition (Left inverse and right inverse of function)
is-left-inverse is-right-inverse : f ∶ A ⟶ B → (g : set) → Set
is-left-inverse {f} {A} {B} f-fun g = g ∶ B ⟶ A ∧ g ∘ f ≡ id A
is-right-inverse {f} {A} {B} f-fun g = g ∶ B ⟶ A ∧ f ∘ g ≡ id B

open ≡-Reasoning
open import ZF.Foundation.Axiom.Nonconstructive

left-inv⇔inj : (f-fun : f ∶ A ⟶ B)(A≡∅→B≡∅ : A ≡ ∅ → B ≡ ∅)
  → --------------------------------------------------------------
  ∃ (is-left-inverse f-fun) ⇔ fun-injective f A
left-inv⇔inj {f}{A}{B} f-fun A≡∅→B≡∅  = mk⇔
  (λ {(g , g-fun , gf≡id) {a} a∈A {a'} a'∈A fa≡fa' →
    begin a ≡⟨ sym $ id[]≡ a∈A ⟩
          id A [ a ] ≡⟨ cong (_[ a ]) $ sym gf≡id ⟩
          g ∘ f [ a ] ≡⟨ ∘[]≡ g-fun f-fun a∈A  ⟩
          g [ f [ a ] ] ≡⟨ cong (g [_]) fa≡fa' ⟩
          g [ f [ a' ] ] ≡⟨ sym $ ∘[]≡ g-fun f-fun a'∈A ⟩
          g ∘ f [ a' ] ≡⟨ cong (_[ a' ]) gf≡id ⟩
          id A [ a' ] ≡⟨ id[]≡ a'∈A ⟩
          a' ∎})
  (λ finj-f → case is? (inhabited A) of λ
    { (yes A≢∅@(a , a∈A)) →
      let rest = (B - ran f) × ｛ a ｝
          f-rel = fun→rel f-fun
          inj-f : injective f
          inj-f = from (fun-inj⇔inj f-fun) λ {x} → finj-f {x}
          f⁻¹-fun : f ⁻¹ ∶ ran f ⟶ A
          f⁻¹-fun = Bijection.⁻¹-fun (⟶-tighten f-fun) inj-f triv-surj
          g = f ⁻¹ ∪ rest
          g-fun : g ∶ B ⟶ A
          g-fun = ∶⟶-cong-⊆
            (A⊆B→A∪[B-A]≡B $ ran⊆ f-rel)
            (A⊆X∧B⊆X→A∪B⊆X refl-⊆ ((λ { refl → a∈A}) F.∘ from ∈｛ a ｝)) $
            ∪-fun disjoint-A-[B-A] f⁻¹-fun const-fun
          g'≡f⁻¹ : Fun.restrict g (ran f) ≡ f ⁻¹
          g'≡f⁻¹ = fun-ext (restrict-fun (ran⊆ f-rel) g-fun) f⁻¹-fun
            λ {x} x∈ranf → case from ∈ran x∈ranf of λ {(y , xy∈f) → begin
            Fun.restrict g (ran f) [ x ]
              ≡⟨ restrict-to g-fun [ to ∈∩ (ran⊆ f-rel x∈ranf , x∈ranf) ]≡ ⟩
            g [ x ] ≡⟨ proj₂ $ to (g-fun [ x ]≡) $ to ∈∪ $ inj₁ $ to ⟨,⟩∈⁻¹ xy∈f ⟩
            y ≡⟨ sym $ proj₂ $ to (f⁻¹-fun [ x ]≡) $ to ⟨,⟩∈⁻¹ xy∈f ⟩
            f ⁻¹ [ x ]  ∎}
      in
      g , g-fun , (
      begin g ∘ f ≡⟨ restrict-∘ ⟩
            Fun.restrict g (ran f) ∘ restrict f (dom f)(dom g)
              ≡⟨ cong₂ (λ f⁻¹ B → f⁻¹ ∘ restrict f (dom f) B)
                       g'≡f⁻¹ (proj₁ g-fun) ⟩
            f ⁻¹ ∘ restrict f (dom f) B
              ≡⟨ cong (f ⁻¹ ∘_) $
                 restrict-transparent-⊆ f-rel refl-⊆ $ ran⊆ f-rel ⟩
            f ⁻¹ ∘ f ≡⟨ f⁻¹∘f≡id f-fun inj-f ⟩
            id A ∎)
    ; (no ¬∃a∈A) →
      let A≡∅ = ¬inhabited≡∅ ¬∃a∈A in
      case A≡∅ , A≡∅→B≡∅ A≡∅ of λ { (refl , refl) → ∅ , ∅-fun , (
        begin ∅ ∘ f ≡⟨ ∘-∅ˡ ⟩
              ∅ ≡⟨ sym id-∅ ⟩
              id ∅ ∎)}})

open import ZF.Foundation.Axiom.Choice

right-inv⇔surj : (f-fun : f ∶ A ⟶ B)
  → --------------------------------------------------------------
  ∃ (is-right-inverse f-fun) ⇔ surjective f B
right-inv⇔surj {f}{A}{B} f-fun@(_ , _ , f-rel) = mk⇔
  (λ { (g , g-fun , fg≡id) {y} y∈B → g [ y ] , from (f-fun [ g [ y ] ]≡)
    (val∈ran g-fun y∈B ,
    (begin f [ g [ y ] ] ≡⟨ sym $ ∘[]≡ f-fun g-fun y∈B ⟩
           f ∘ g [ y ] ≡⟨ cong (_[ y ]) fg≡id ⟩
           id B [ y ] ≡⟨ id[]≡ y∈B ⟩
           y ∎ ))})
  λ surj-f → let
    g₀ = f-rel ⃖
    g₀-fun : g₀ ∶ B ⟶ ran g₀
    g₀-fun = ⟶-tighten $ ⃖-fun f-rel
    ∅∉ran-g₀ : ∅ ∉ ran g₀
    ∅∉ran-g₀ ∅∈ran-g₀ = case from ∈ran ∅∈ran-g₀ of λ { (y , y∅∈g₀) →
      let y∈B = dom⊆ (fun→rel g₀-fun) $ to ∈dom $ ∅ , y∅∈g₀
          sep≡∅ : ｛ x ∈ A ∣ f [ x ] ≡ y ｝ ≡ ∅
          sep≡∅ = begin
            ｛ x ∈ A ∣ f [ x ] ≡ y ｝ ≡⟨ sym $ ⃖-fvalid f-fun y∈B ⟩
            g₀ [ y ] ≡⟨ proj₂ $ to (g₀-fun [ y ]≡) y∅∈g₀ ⟩
            ∅ ∎
      in case surj-f y∈B of λ { (x , xy∈f) →
      ∈∅ $ subst (x ∈_) sep≡∅ $ to ∈｛ x ∈ A ∣ f [ x ] ≡ y ｝ $
      to (f-fun [ x ]≡) xy∈f  }}
    g₁ = elem $ choice-fun ∅∉ran-g₀
    g = g₁ ∘ g₀
    g₁-fun : g₁ ∶ ran g₀ ⟶ A
    g₁-fun = proj₁ (prop $ choice-fun ∅∉ran-g₀) ∶ (g₁ ∶ ran g₀ ⟶ ⋃ (ran g₀))
      |> ∶⟶-cong-⊆ refl (⋃-⊆ λ {X} X∈ran-g₀ {x} x∈X →
           ran⊆ (fun→rel $ ⃖-fun f-rel) X∈ran-g₀ ∶ X ∈ 𝒫 A
           |> from ∈𝒫 ∶ X ⊆ A
           |> (λ X⊆A → X⊆A x∈X) ∶ x ∈ A )
    g-fun : g ∶ B ⟶ A
    g-fun = ∘-fun g₁-fun g₀-fun
    fg≡id : f ∘ g ≡ id B
    fg≡id = fun-ext (∘-fun f-fun g-fun) id-fun λ {y} y∈B →
      let S = ｛ x ∈ A ∣ f [ x ] ≡ y ｝
          ∈S = ∈｛ x ∈ A ∣ f [ x ] ≡ y ｝
          g₀[y]≡S = ⃖-fvalid f-fun y∈B
          g₁[S]∈S : g₁ [ S ] ∈ S
          g₁[S]∈S = proj₂ (prop $ choice-fun ∅∉ran-g₀) $
                    subst (_∈ ran g₀) g₀[y]≡S $
                    val∈ran g₀-fun y∈B
      in begin
      f ∘ g [ y ] ≡⟨ ∘[]≡ f-fun g-fun y∈B ⟩
      f [ g₁ ∘ g₀ [ y ] ] ≡⟨ cong (f [_]) $ ∘[]≡ g₁-fun g₀-fun y∈B ⟩
      f [ g₁ [ g₀ [ y ] ] ] ≡⟨ cong (λ S → f [ g₁ [ S ] ]) g₀[y]≡S ⟩
      f [ g₁ [ S ] ] ≡⟨ proj₂ $ from ∈S g₁[S]∈S ⟩
      y ≡⟨ sym $ id[]≡ y∈B ⟩
      id B [ y ] ∎
    in g , g-fun , fg≡id
