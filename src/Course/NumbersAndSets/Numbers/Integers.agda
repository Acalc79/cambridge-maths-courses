{-# OPTIONS --exact-split #-}
open import Course.NumbersAndSets.Numbers.Naturals

module Course.NumbersAndSets.Numbers.Integers (nat : Peanoℕ) where

open WithPeano nat
open import Course.NumbersAndSets.Base
open import Course.NumbersAndSets.Relation

⟨_,_⟩~ℤ⟨_,_⟩ : (a b c d : set) → Set
⟨ a , b ⟩~ℤ⟨ c , d ⟩ = a + d ≡ c + b

private
  ϕ : (ab cd : set) → Set
ϕ ab cd = ∃₂ λ a b → ⟨ a ، b ⟩ ≡ ab ∧
          ∃₂ λ c d → ⟨ c ، d ⟩ ≡ cd ∧ ⟨ a , b ⟩~ℤ⟨ c , d ⟩

~ℤ : set
~ℤ = reify (ℕ × ℕ)(ℕ × ℕ) ϕ

open import Logic.Properties
open SetVars

module _ where
  open import Function.Equivalence.Reasoning
  ∈~ℤ : ⟨ x ، y ⟩ ∈ ~ℤ ⇔
    (⋁ a ∈ ℕ , ⋁ b ∈ ℕ , ⋁ c ∈ ℕ , ⋁ d ∈ ℕ ,
      ⟨ a ، b ⟩ ≡ x ∧ ⟨ c ، d ⟩ ≡ y ∧ ⟨ a , b ⟩~ℤ⟨ c , d ⟩)
  ∈~ℤ {x}{y} = begin
    ⟨ x ، y ⟩ ∈ ~ℤ ⇔⟨ ⟨,⟩∈reify ⟩
    (x ∈ ℕ × ℕ ∧ y ∈ ℕ × ℕ ∧ ϕ x y)
      ⇔⟨ mk⇔ (λ { (x∈ℕ2 , y∈ℕ2 , a , b , refl , c , d , refl , ab~cd) →
                case from ∈× x∈ℕ2 , from ∈× y∈ℕ2 of λ
             { ((a' , a'∈ℕ , b' , b'∈ℕ , ab≡a'b') ,
                (c' , c'∈ℕ , d' , d'∈ℕ , cd≡c'd')) →
                case from ⟨,⟩≡ ab≡a'b' , from ⟨,⟩≡ cd≡c'd' of λ
             { ((refl , refl) , refl , refl) →
               a , a'∈ℕ , b , b'∈ℕ , c , c'∈ℕ , d , d'∈ℕ , refl , refl , ab~cd}}})
             (λ { (a , a∈ℕ , b , b∈ℕ , c , c∈ℕ , d , d∈ℕ , refl , refl , ab~cd) →
               to ⟨,⟩∈× (a∈ℕ , b∈ℕ) , to ⟨,⟩∈× (c∈ℕ , d∈ℕ) ,
               a , b , refl , c , d , refl , ab~cd}) ⟩
    (⋁ a ∈ ℕ , ⋁ b ∈ ℕ , ⋁ c ∈ ℕ , ⋁ d ∈ ℕ ,
      ⟨ a ، b ⟩ ≡ x ∧ ⟨ c ، d ⟩ ≡ y ∧ ⟨ a , b ⟩~ℤ⟨ c , d ⟩) ∎
  
  ⟨,⟩∈~ℤ : ⋀ a ∈ ℕ , ⋀ b ∈ ℕ , ⋀ c ∈ ℕ , ⋀ d ∈ ℕ ,
    (⟨ ⟨ a ، b ⟩ ، ⟨ c ، d ⟩ ⟩ ∈ ~ℤ ⇔ a + d ≡ c + b)
  ⟨,⟩∈~ℤ {a} a∈ℕ {b} b∈ℕ {c} c∈ℕ {d} d∈ℕ = begin
    ⟨ ⟨ a ، b ⟩ ، ⟨ c ، d ⟩ ⟩ ∈ ~ℤ ⇔⟨ ∈~ℤ ⟩
    (⋁ a' ∈ ℕ , ⋁ b' ∈ ℕ , ⋁ c' ∈ ℕ , ⋁ d' ∈ ℕ ,
      ⟨ a' ، b' ⟩ ≡ ⟨ a ، b ⟩ ∧ ⟨ c' ، d' ⟩ ≡ ⟨ c ، d ⟩ ∧
      ⟨ a' , b' ⟩~ℤ⟨ c' , d' ⟩)
      ⇔⟨ mk⇔ (λ { (a' , a'∈ℕ , b' , b'∈ℕ , c' , c'∈ℕ , d' , d'∈ℕ ,
                   a'b'≡ab , c'd'≡cd , a'b'~c'd') →
                   case from ⟨,⟩≡ a'b'≡ab , from ⟨,⟩≡ c'd'≡cd of λ
                { ((refl , refl) , refl , refl) → a'b'~c'd'}})
             (λ ab~cd → a , a∈ℕ , b , b∈ℕ , c , c∈ℕ , d , d∈ℕ , refl , refl , ab~cd) ⟩
    ⟨ a , b ⟩~ℤ⟨ c , d ⟩ ≡⟨⟩
    a + d ≡ c + b ∎

open RelDefs ~ℤ (ℕ × ℕ)
open ≡-Reasoning
open Nat-Solver using (solve; _:+_; _:=_)
import Data.Nat as Nat

equiv~ℤ : equivalence
equiv~ℤ =
  (λ x∈ℕ2 → case from ∈× x∈ℕ2 of λ
    { (x , x∈ℕ , y , y∈ℕ , refl) → to (⟨,⟩∈~ℤ x∈ℕ y∈ℕ x∈ℕ y∈ℕ) refl}) ,
  (λ x~y → case from ∈~ℤ x~y of λ
    { (a , a∈ℕ , b , b∈ℕ , c , c∈ℕ , d , d∈ℕ , refl , refl , ab~cd) →
      to (⟨,⟩∈~ℤ c∈ℕ d∈ℕ a∈ℕ b∈ℕ) $ sym ab~cd}) ,
  λ x~y y~z → case from ∈~ℤ x~y , from ∈~ℤ y~z of λ
    { ((a , a∈ℕ , b , b∈ℕ , c , c∈ℕ , d , d∈ℕ , refl , refl , ab~cd) ,
       (c' , c'∈ℕ , d' , d'∈ℕ , e , e∈ℕ , f , f∈ℕ , cd≡c'd' , refl , c'd'~ef)) →
       case from ⟨,⟩≡ cd≡c'd' of λ { (refl , refl) →
       to (⟨,⟩∈~ℤ a∈ℕ b∈ℕ e∈ℕ f∈ℕ) $
       +-cancelʳ (+∈ℕ a∈ℕ f∈ℕ) (+∈ℕ e∈ℕ b∈ℕ) d∈ℕ $ begin
         a + f + d ≡⟨ solve 3 (λ a f d → a :+ f :+ d := a :+ d :+ f) refl
                      [el: a ∈: a∈ℕ ] [el: f ∈: f∈ℕ ] [el: d ∈: d∈ℕ ] ⟩
         a + d + f ≡⟨ cong (_+ f) ab~cd ⟩
         c + b + f ≡⟨ solve 3 (λ c b f → c :+ b :+ f := c :+ f :+ b) refl
                      [el: c ∈: c∈ℕ ] [el: b ∈: b∈ℕ ] [el: f ∈: f∈ℕ ] ⟩
         c + f + b ≡⟨ cong (_+ b) c'd'~ef ⟩
         e + d + b ≡⟨ solve 3 (λ e d b → e :+ d :+ b := e :+ b :+ d) refl
                      [el: e ∈: e∈ℕ ] [el: d ∈: d∈ℕ ] [el: b ∈: b∈ℕ ] ⟩
         e + b + d ∎}}

ℤ : set
ℤ = ｛ ⟦ x ⟧ ∣ x ∈ ℕ × ℕ ｝
