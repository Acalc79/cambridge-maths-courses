{-# OPTIONS --exact-split #-}
module Course.NumbersAndSets.Numbers.Naturals where

open import Course.NumbersAndSets.Numbers.Naturals.Axioms public
open import Course.NumbersAndSets.Numbers.Naturals.Equivalence public
open import Course.NumbersAndSets.Numbers.Naturals.Omega public

module Generic where
  open import Course.NumbersAndSets.Numbers.Naturals.Recursion public
  open import Course.NumbersAndSets.Numbers.Naturals.Arithmetic public
  open import Course.NumbersAndSets.Numbers.Naturals.Solver public

module WithPeano (nat : Peanoℕ) where
  open Peanoℕ nat public
  open import Course.NumbersAndSets.Numbers.Naturals.Recursion nat public
  open import Course.NumbersAndSets.Numbers.Naturals.Arithmetic nat public
  open import Course.NumbersAndSets.Numbers.Naturals.Solver nat public
