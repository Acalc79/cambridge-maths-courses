{-# OPTIONS --exact-split #-}
open import Course.NumbersAndSets.Numbers.Naturals.Axioms

module Course.NumbersAndSets.Numbers.Naturals.Recursion (nat : Peanoℕ) where

open import Course.NumbersAndSets.Base

open Peanoℕ nat
open ≡-Reasoning

module ω-ℕ where
  open F.RecDef (zero ≡_) (λ _ n v → suc n ≡ v)
                (zero , refl , λ zero≡y → zero≡y)
                (λ _ n → suc n , refl , λ n+1≡y → n+1≡y)
  ω→ℕ = f

  ℕ→ω = ω→ℕ ⁻¹

  ω→ℕ-fun' : f ∶ ω ⟶ ran f
  ω→ℕ-fun' = subst (f ∶_⟶ ran f) f-dom $ is-set-fun→∶⟶ f-is-fun

  f[x]∈ℕ : ⋀ x ∈ ω , f [ x ] ∈ ℕ
  f[x]∈ℕ = f [ x ] ∈ ℕ by-induction-on x
    [base: subst (_∈ ℕ) f[0] 0∈ℕ
    ,step: (λ {x} x∈ω f[x]∈ℕ →
      subst (_∈ ℕ)(f[x⁺] x∈ω) $ suc∈ℕ f[x]∈ℕ) ]

  ranf⊆ : ran f ⊆ ℕ
  ranf⊆ {n} n∈ranf = case from ∈ran n∈ranf of λ
    { (x , xn∈f) →
      let f[x]≡n = proj₂ $ to (ω→ℕ-fun' [ x ]≡) xn∈f
          x∈ω = dom⊆ (fun→rel ω→ℕ-fun') $ to ∈dom (n , xn∈f)
      in subst (_∈ ℕ) f[x]≡n $ f[x]∈ℕ x∈ω}

  ran-ω→ℕ : ran f ≡ ℕ
  ran-ω→ℕ = ranf⊆ ≡ℕ-by-induction,base:
      to ∈ran $ ∅ , from (ω→ℕ-fun' [ ∅ ]≡) (∅∈ω , sym f[0])
    inductive-step: λ {n} n∈ranf → case from ∈ran n∈ranf of λ
      { (x , xn∈f) → to ∈ran $ x ⁺ ,
        let x∈ω = dom⊆ (fun→rel ω→ℕ-fun') $ to ∈dom (n , xn∈f) in
        from (ω→ℕ-fun' [ x ⁺ ]≡) (
          ⁺∈ω x∈ω , (
          begin f [ x ⁺ ] ≡⟨ sym $ f[x⁺] x∈ω ⟩
                suc (f [ x ]) ≡⟨ cong suc $ proj₂ $ to (ω→ℕ-fun' [ x ]≡) xn∈f ⟩
                suc n ∎)) }
  
  ω→ℕ-fun : f ∶ ω ⟶ ℕ
  ω→ℕ-fun = subst (ω→ℕ ∶ ω ⟶_) ran-ω→ℕ ω→ℕ-fun'

  inj-ω→ℕ : injective f
  inj-ω→ℕ = from (fun-inj⇔inj ω→ℕ-fun) $
    (⋀ x' ∈ ω , (f [ x ] ≡ f [ x' ] → x ≡ x')) by-induction-on x
    [base: (λ x'∈ω f0≡fx' → case from ∈ω x'∈ω of λ
      { (inj₁ refl) → refl
      ; (inj₂ (x″ , x″∈ω , refl)) → ⊥-elim $ f[x⁺]≢f[0] x″∈ω $ sym f0≡fx'})
    ,step: (λ {x} x∈ω IH {x'} x'∈ω fx⁺≡fx' → case from ∈ω x'∈ω of λ
      { (inj₁ refl) → ⊥-elim $ f[x⁺]≢f[0] x∈ω fx⁺≡fx'
      ; (inj₂ (x″ , x″∈ω , refl)) → cong _⁺ $ IH x″∈ω $
        S-inj (val∈ran ω→ℕ-fun x∈ω) (val∈ran ω→ℕ-fun x″∈ω) $
        begin suc (f [ x ]) ≡⟨ f[x⁺] x∈ω ⟩
              f [ x ⁺ ] ≡⟨ fx⁺≡fx' ⟩
              f [ x″ ⁺ ] ≡⟨ sym $ f[x⁺] x″∈ω ⟩
              suc (f [ x″ ]) ∎}) ]
    where
    f[x⁺]≢f[0] : ⋀ x ∈ ω , f [ x ⁺ ] ≢ f [ ∅ ]
    f[x⁺]≢f[0] {x} x∈ω fx⁺≡f0 =
      let fx∈ℕ = val∈ran ω→ℕ-fun x∈ω
      in suc≢0 fx∈ℕ $
      begin suc (f [ x ]) ≡⟨ f[x⁺] x∈ω ⟩
            f [ x ⁺ ] ≡⟨ fx⁺≡f0 ⟩
            f [ ∅ ] ≡⟨ sym f[0] ⟩
            zero ∎

  surj-ω→ℕ : surjective f ℕ
  surj-ω→ℕ = from (fun-surj⇔surj ω→ℕ-fun) $
    (⋁ x ∈ ω , f [ x ] ≡ n) by-ℕ-induction-on n
    [base: ∅ , ∅∈ω , sym f[0]
    ,step: (λ { {n} n∈ℕ (x , x∈ω , refl) →
      x ⁺ , ⁺∈ω x∈ω , sym (f[x⁺] x∈ω)}) ] 

  open Bijection ω→ℕ-fun inj-ω→ℕ surj-ω→ℕ

  ℕ→ω-fun : ℕ→ω ∶ ℕ ⟶ ω
  ℕ→ω-fun = ⁻¹-fun

  ℕ→ω[0] : ℕ→ω [ zero ] ≡ ∅
  ℕ→ω[0] = sym $ f[x]≡y→x≡f⁻¹[y] ∅∈ω $ sym f[0]

  ℕ→ω[suc] : ⋀ n ∈ ℕ , ℕ→ω [ suc n ] ≡ (ℕ→ω [ n ]) ⁺
  ℕ→ω[suc] {n} n∈ℕ =
    let ℕ→ω[n]∈ω = val∈ran ℕ→ω-fun n∈ℕ in
    sym $ f[x]≡y→x≡f⁻¹[y] (⁺∈ω ℕ→ω[n]∈ω) $
    begin ω→ℕ [ (ℕ→ω [ n ]) ⁺ ] ≡⟨ sym $ f[x⁺] ℕ→ω[n]∈ω ⟩
          suc (ω→ℕ [ ℕ→ω [ n ] ]) ≡⟨ cong suc $ ff⁻¹[y]≡y n∈ℕ ⟩
          suc n ∎

  ω↔ℕ : bijection ω→ℕ-fun
  ω↔ℕ = bij

module RecDef (f0 : set)(f[suc-n] : (n f[n] : set) → set) where
  open ω-ℕ using (ω→ℕ; ω→ℕ-fun)
  open WithBijection ω→ℕ-fun ω-ℕ.ω↔ℕ
    renaming (f⁻¹ to ℕ→ω; f⁻¹-fun to ℕ→ω-fun)

  private
    module on-ω =
      F.RecDef (f0 ≡_) (λ n f[n] v → f[suc-n] (ω→ℕ [ n ]) f[n] ≡ v)
               (f0 , refl , λ f[0]≡y → f[0]≡y)
               (λ n f[n] → f[suc-n] (ω→ℕ [ n ]) f[n] , refl , λ f[n+1]≡y → f[n+1]≡y)
    open on-ω renaming (f to f') using ()

    f'-fun : f' ∶ ω ⟶ ran f'
    f'-fun = subst (f' ∶_⟶ ran f') on-ω.f-dom $
             is-set-fun→∶⟶ on-ω.f-is-fun

  f = f' ∘ ℕ→ω

  abstract
    f-fun : f ∶ ℕ ⟶ ran f
    f-fun = ⟶-tighten $ ∘-fun f'-fun ℕ→ω-fun

    f[0] : f [ zero ] ≡ f0
    f[0] = begin
      f' ∘ ℕ→ω [ zero ] ≡⟨ ∘[]≡ f'-fun ℕ→ω-fun 0∈ℕ  ⟩
      f' [ ℕ→ω [ zero ] ] ≡⟨ cong (f' [_]) ω-ℕ.ℕ→ω[0] ⟩
      f' [ ∅ ] ≡⟨ sym on-ω.f[0] ⟩
      f0 ∎

    f[suc] : ⋀ n ∈ ℕ , f [ suc n ] ≡ f[suc-n] n (f [ n ])
    f[suc] {n} n∈ℕ = begin
      f' ∘ ℕ→ω [ suc n ] ≡⟨ ∘[]≡ f'-fun ℕ→ω-fun (suc∈ℕ n∈ℕ)  ⟩
      f' [ ℕ→ω [ suc n ] ] ≡⟨ cong (f' [_]) $ ω-ℕ.ℕ→ω[suc] n∈ℕ ⟩
      f' [ (ℕ→ω [ n ]) ⁺ ] ≡⟨ sym $ on-ω.f[x⁺] $ val∈ran ℕ→ω-fun n∈ℕ ⟩
      f[suc-n] (ω→ℕ [ ℕ→ω [ n ] ]) (f' [ ℕ→ω [ n ] ])
        ≡⟨ cong₂ f[suc-n] (ff⁻¹[y]≡y n∈ℕ) $ sym $ ∘[]≡ f'-fun ℕ→ω-fun n∈ℕ ⟩
      f[suc-n] n (f [ n ]) ∎
