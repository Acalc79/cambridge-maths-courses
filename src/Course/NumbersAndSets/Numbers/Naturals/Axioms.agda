{-# OPTIONS --exact-split #-}
module Course.NumbersAndSets.Numbers.Naturals.Axioms where

open import Course.NumbersAndSets.Base

record Peanoℕ : Set where
  field
    ℕ zero S : set
    0∈ℕ : zero ∈ ℕ
    S-fun : S ∶ ℕ ⟶ ℕ

  suc = S [_]
  one = suc zero
  two = suc one

  field
    suc≢0 : ⋀ n ∈ ℕ , suc n ≢ zero
    S-inj : fun-injective S ℕ
    induction : ∀{A} → A ⊆ ℕ → zero ∈ A → (⋀ n ∈ A , suc n ∈ A) → A ≡ ℕ

  _≡ℕ-by-induction,base:_inductive-step:_ = induction

  suc∈ℕ : ⋀ n ∈ ℕ , suc n ∈ ℕ
  suc∈ℕ = val∈ran S-fun

  infix 0 prop-induction-syntax
  prop-induction-syntax : (P : set → Set) →
    P zero →
    ⋀ n ∈ ℕ , (P n → P (suc n))
    → ---------------------------------------
    ⋀ n ∈ ℕ , P n
  prop-induction-syntax P P0 P+ {x} x∈ℕ =
    proj₂ $ from ∈A $ subst (x ∈_) (sym A≡ℕ) x∈ℕ
    where
    A = ｛ n ∈ ℕ ∣ P n ｝
    ∈A = ∈｛ n ∈ ℕ ∣ P n ｝
    A⊆ℕ : A ⊆ ℕ
    A⊆ℕ = sep⊆ P ℕ
    A≡ℕ = A⊆ℕ ≡ℕ-by-induction,base:
      to ∈A $ 0∈ℕ , P0
      inductive-step: λ x∈A → case from ∈A x∈A of λ
        { (x∈ℕ , Px) → to ∈A $ suc∈ℕ x∈ℕ , P+ x∈ℕ Px }

  syntax prop-induction-syntax (λ n → P) p0 p+ =
    P by-ℕ-induction-on n [base: p0 ,step: p+ ]

  ℕ-cases : ⋀ n ∈ ℕ , zero ≡ n ∨ (⋁ m ∈ ℕ , suc m ≡ n)
  ℕ-cases = (zero ≡ n ∨ (⋁ m ∈ ℕ , suc m ≡ n)) by-ℕ-induction-on n
    [base: inj₁ refl
    ,step: (λ { n∈ℕ (inj₁ refl) → inj₂ $ zero , 0∈ℕ , refl
              ; n∈ℕ (inj₂ (m , m∈ℕ , refl)) → inj₂ $
                suc m , suc∈ℕ m∈ℕ , refl}) ]

  open import Data.Sum

  _≟_ : ⋀ n ∈ ℕ , ⋀ m ∈ ℕ , Dec (n ≡ m)
  _≟_ = (⋀ m ∈ ℕ , Dec (n ≡ m)) by-ℕ-induction-on n
    [base: (λ m∈ℕ →
      [ yes , (λ { (m , m∈ℕ , refl) → no $ suc≢0 m∈ℕ F.∘ sym}) ] $
      ℕ-cases m∈ℕ)
    ,step: (λ n∈ℕ IH m∈ℕ →
      [ (λ { refl → no $ suc≢0 n∈ℕ}) ,
        (λ { (m' , m'∈ℕ , refl) → case IH m'∈ℕ of λ
           { (yes refl) → yes refl
           ; (no n≢m) → no λ Sn≡Sm → n≢m $ S-inj n∈ℕ m'∈ℕ Sn≡Sm}}) ] $
      ℕ-cases m∈ℕ) ]
