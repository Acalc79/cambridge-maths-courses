{-# OPTIONS --exact-split #-}
open import Course.NumbersAndSets.Numbers.Naturals.Axioms

module Course.NumbersAndSets.Numbers.Naturals.Literal (nat : Peanoℕ) where
open Peanoℕ nat

open import Course.NumbersAndSets.Base

import Agda.Builtin.Nat as Nat
open import Agda.Builtin.FromNat public
open import Data.Unit using (⊤)
open Data.Unit using (tt) public

instance
  number-ℕ : Number set
Number.Constraint number-ℕ _ = ⊤
fromNat ⦃ number-ℕ ⦄ 0 = zero
fromNat ⦃ number-ℕ ⦄ (Nat.suc n) = suc (fromNat ⦃ number-ℕ ⦄ n)
