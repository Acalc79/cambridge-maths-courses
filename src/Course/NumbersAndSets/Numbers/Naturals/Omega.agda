{-# OPTIONS --exact-split #-}
module Course.NumbersAndSets.Numbers.Naturals.Omega where

open import Course.NumbersAndSets.Base
open import Course.NumbersAndSets.Numbers.Naturals.Axioms

open Peanoℕ

ω-suc : set
ω-suc = fun-reify _⁺ ω

ran-ω-suc : ran ω-suc ⊆ ω
ran-ω-suc y∈ran = case from ∈ran-fun-reify y∈ran of λ
  { (x , x∈ω , refl) → ⁺∈ω x∈ω}

open ≡-Reasoning

_⁻ : set → set
x ⁻ = ⋃ x

_⁺⁻ : ⋀ x ∈ ω , x ⁺ ⁻ ≡ x
_⁺⁻ {x} x∈ω = antisym-⊆
  (λ {a} a∈x⁺⁻ → case from ∈⋃ a∈x⁺⁻ of λ
    { (y , y∈x⁺ , a∈y) → case from ∈⁺ y∈x⁺ of λ
    { (inj₁ y∈x) → x∈ω→transitive-x x∈ω y∈x a∈y
    ; (inj₂ refl) → a∈y }})
  (λ {a} a∈x → to ∈⋃ $ x , to ∈⁺ (inj₂ refl) , a∈x)

ω-Peano : Peanoℕ
ℕ ω-Peano = ω
zero ω-Peano = ∅
S ω-Peano = ω-suc
0∈ℕ ω-Peano = ∅∈ω
S-fun ω-Peano = ∶⟶-cong-⊆ refl ran-ω-suc fun-fun-reify
suc≢0 ω-Peano {x} x∈ω ω-sucx≡∅ = ⁺≢∅ $
  begin x ⁺ ≡⟨ sym $ valid-fun-reify x∈ω ⟩
        ω-suc [ x ] ≡⟨ ω-sucx≡∅ ⟩
        ∅ ∎ 
S-inj ω-Peano {x} x∈ω {x'} x'∈ω sucx≡sucx' = begin
  x ≡⟨ sym $ x∈ω ⁺⁻ ⟩
  x ⁺ ⁻ ≡⟨ cong _⁻ $ sym $ valid-fun-reify x∈ω ⟩
  (ω-suc [ x ]) ⁻ ≡⟨ cong _⁻ sucx≡sucx' ⟩
  (ω-suc [ x' ]) ⁻ ≡⟨ cong _⁻ $ valid-fun-reify x'∈ω ⟩
  x' ⁺ ⁻ ≡⟨ x'∈ω ⁺⁻ ⟩
  x' ∎
induction ω-Peano {A} A⊆ω ∅∈A ⁺∈A = antisym-⊆ A⊆ω $
  x ∈ A by-induction-on x
  [base: ∅∈A
  ,step: (λ x∈ω IH → subst (_∈ A) (valid-fun-reify x∈ω) $ ⁺∈A IH) ] 
