{-# OPTIONS --exact-split #-}
open import Course.NumbersAndSets.Numbers.Naturals.Axioms

module Course.NumbersAndSets.Numbers.Naturals.Solver (nat : Peanoℕ) where

import Algebra.Solver.Ring.Simple as Solver
import Algebra.Solver.Ring.AlmostCommutativeRing as ACR

open import Course.NumbersAndSets.Base
import Course.NumbersAndSets.Numbers.Naturals.Arithmetic nat as A
private module Nat = Peanoℕ nat
open Nat using (ℕ; 0∈ℕ; zero)

------------------------------------------------------------------------
-- A module for automatically solving propositional equivalences
-- containing _+_ and _*_

open import Algebra.Structures {A = ElemOf ℕ} _≈_
open import Function
  
module ElemOfℕ where
  _≟_ : (x' y' : ElemOf ℕ) → Dec (x' ≈ y')
  [el: x ∈: x∈ℕ ] ≟ [el: y ∈: y∈ℕ ] = Peanoℕ._≟_ nat x∈ℕ y∈ℕ

  infixl 31 _*_
  infixl 30 _+_

  _*_ _+_ : (x' y' : ElemOf ℕ) → ElemOf ℕ
  [el: x ∈: x∈ℕ ] + [el: y ∈: y∈ℕ ] = [el: x A.+ y ∈: A.+∈ℕ x∈ℕ y∈ℕ ]
  [el: x ∈: x∈ℕ ] * [el: y ∈: y∈ℕ ] = [el: x A.* y ∈: A.*∈ℕ x∈ℕ y∈ℕ ]

open ElemOfℕ

+-isMagma : IsMagma _+_
+-isMagma = record
  { isEquivalence = ≈-isEquivalence
  ; ∙-cong        = λ { refl refl → refl }
  }

+-isSemigroup : IsSemigroup _+_
+-isSemigroup = record
  { isMagma = +-isMagma
  ; assoc   = λ { x [el: y ∈: y∈ℕ ] [el: z ∈: z∈ℕ ] → sym $ A.+-assoc y∈ℕ z∈ℕ}
  }

0-el : ElemOf ℕ
0-el = [el: zero ∈: 0∈ℕ ]

+-0-isMonoid : IsMonoid _+_ 0-el
+-0-isMonoid = record
  { isSemigroup = +-isSemigroup
  ; identity    = (λ { [el: x ∈: x∈ℕ ] → A.0-+ x∈ℕ}) , λ _ → A.+-0
  }

+-0-isCommutativeMonoid : IsCommutativeMonoid _+_ 0-el
+-0-isCommutativeMonoid = record
  { isMonoid = +-0-isMonoid
  ; comm     = λ { [el: x ∈: x∈ℕ ] [el: y ∈: y∈ℕ ] → A.+-comm x∈ℕ y∈ℕ}
  }

*-isMagma : IsMagma _*_
*-isMagma = record
  { isEquivalence = ≈-isEquivalence
  ; ∙-cong        = λ { refl refl → refl }
  }

*-isSemigroup : IsSemigroup _*_
*-isSemigroup = record
  { isMagma = *-isMagma
  ; assoc   = λ { [el: x ∈: x∈ℕ ] [el: y ∈: y∈ℕ ] [el: z ∈: z∈ℕ ] →
                  sym $ A.*-assoc x∈ℕ y∈ℕ z∈ℕ}
  }

1-el : ElemOf ℕ
1-el = [el: Nat.suc zero ∈: Nat.suc∈ℕ 0∈ℕ ]

*-1-isMonoid : IsMonoid _*_ 1-el
*-1-isMonoid = record
  { isSemigroup = *-isSemigroup
  ; identity    = (λ { [el: x ∈: x∈ℕ ] → A.1-* x∈ℕ}) ,
                  (λ { [el: x ∈: x∈ℕ ] → A.*-1 x∈ℕ})
  }

+-*-isSemiring : IsSemiring _+_ _*_ 0-el 1-el
+-*-isSemiring = record
  { isSemiringWithoutAnnihilatingZero = record
    { +-isCommutativeMonoid = +-0-isCommutativeMonoid
    ; *-isMonoid            = *-1-isMonoid
    ; distrib               =
      (λ { [el: x ∈: x∈ℕ ] [el: y ∈: y∈ℕ ] [el: z ∈: z∈ℕ ] →
        A.*-distribˡ-+ x∈ℕ y∈ℕ z∈ℕ}) ,
      (λ { [el: x ∈: x∈ℕ ] [el: y ∈: y∈ℕ ] [el: z ∈: z∈ℕ ] →
        A.*-distribʳ-+ y∈ℕ z∈ℕ x∈ℕ})
    }
  ; zero = (λ { [el: x ∈: x∈ℕ ] → A.0-* x∈ℕ}) , λ _ → A.*-0
  }

+-*-isCommutativeSemiring : IsCommutativeSemiring _+_ _*_ 0-el 1-el
+-*-isCommutativeSemiring = record
  { isSemiring = +-*-isSemiring
  ; *-comm     = λ { [el: x ∈: x∈ℕ ] [el: y ∈: y∈ℕ ] → A.*-comm x∈ℕ y∈ℕ}
  }

open import Algebra.Structures {A = ElemOf ℕ} _≈_
open import Algebra

+-*-commutativeSemiring : CommutativeSemiring _ _
+-*-commutativeSemiring = record
  { isCommutativeSemiring = +-*-isCommutativeSemiring
  }

module Nat-Solver = Solver (ACR.fromCommutativeSemiring +-*-commutativeSemiring) _≟_
