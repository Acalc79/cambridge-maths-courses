{-# OPTIONS --exact-split #-}
open import Course.NumbersAndSets.Numbers.Naturals.Axioms

module Course.NumbersAndSets.Numbers.Naturals.Equivalence
  (nat₀ nat₁ : Peanoℕ) where

open import Course.NumbersAndSets.Base

module N₀ = Peanoℕ nat₀
module N₁ = Peanoℕ nat₁

open import Course.NumbersAndSets.Numbers.Naturals.Recursion nat₀
open RecDef N₁.zero (λ _ f[n] → N₁.suc f[n])

N₀→N₁ : set
N₀→N₁ = f

f[n]∈N₁ : ⋀ n ∈ N₀.ℕ , N₀→N₁ [ n ] ∈ N₁.ℕ
f[n]∈N₁ = N₀→N₁ [ n ] ∈ N₁.ℕ by-ℕ-induction-on n
  [base: subst (_∈ N₁.ℕ) (sym f[0]) N₁.0∈ℕ
  ,step: (λ n∈N₀ f[n]∈N₁ →
    subst (_∈ N₁.ℕ) (sym $ f[suc] n∈N₀) $ N₁.suc∈ℕ f[n]∈N₁) ]
  where open N₀

ran⊆N₁ : ran N₀→N₁ ⊆ N₁.ℕ
ran⊆N₁ {x} x∈ran = case from ∈ran x∈ran of λ { (n , nx∈f) →
  case to (f-fun [ n ]≡) nx∈f of λ { (n∈N₀ , refl) →
  f[n]∈N₁ n∈N₀ }}

ran≡N₁ : ran N₀→N₁ ≡ N₁.ℕ
ran≡N₁ = N₁.induction
  ran⊆N₁
  (to ∈ran $ N₀.zero , from (f-fun [ N₀.zero ]≡) (N₀.0∈ℕ , f[0]))
  λ {x} x∈ran → case from ∈ran x∈ran of λ { (n , nx∈f) →
    case to (f-fun [ n ]≡) nx∈f of λ { (n∈N₀ , refl) →
    to ∈ran $ N₀.suc n , from (f-fun [ N₀.suc n ]≡) (
      N₀.suc∈ℕ n∈N₀ ,
      f[suc] n∈N₀)}}

N₀→N₁-fun : N₀→N₁ ∶ N₀.ℕ ⟶ N₁.ℕ
N₀→N₁-fun = subst (N₀→N₁ ∶ N₀.ℕ ⟶_) ran≡N₁ f-fun

open import Data.Sum
open ≡-Reasoning

N₀→N₁-inj : injective N₀→N₁
N₀→N₁-inj = from (fun-inj⇔inj N₀→N₁-fun) $
  (⋀ n' ∈ ℕ , (N₀→N₁ [ n ] ≡ N₀→N₁ [ n' ] → n ≡ n'))
  by-ℕ-induction-on n
  [base: (λ n'∈N₀ fn≡fn' → fromInj₁
    (λ { (m , m∈N₀ , refl) → ⊥-elim $ N₀→N₁[suc]≢N₀→N₁[0] m∈N₀ $ sym fn≡fn' }) $
    ℕ-cases n'∈N₀)
  ,step: (λ {n} n∈N₀ IH n'∈N₀ fn≡fn' → case ℕ-cases n'∈N₀ of λ
    { (inj₁ refl) → ⊥-elim $ N₀→N₁[suc]≢N₀→N₁[0] n∈N₀ fn≡fn'
    ; (inj₂ (m , m∈N₀ , refl)) → cong suc $ IH m∈N₀ $
      N₁.S-inj (val∈ran N₀→N₁-fun n∈N₀)(val∈ran N₀→N₁-fun m∈N₀) $
      begin N₁.suc (N₀→N₁ [ n ]) ≡⟨ sym $ f[suc] n∈N₀ ⟩
            N₀→N₁ [ suc n ] ≡⟨ fn≡fn' ⟩
            N₀→N₁ [ suc m ] ≡⟨ f[suc] m∈N₀ ⟩
            N₁.suc (N₀→N₁ [ m ]) ∎}) ]
  where
  open N₀
  N₀→N₁[suc]≢N₀→N₁[0] : ⋀ m ∈ ℕ , N₀→N₁ [ suc m ] ≢ N₀→N₁ [ zero ]
  N₀→N₁[suc]≢N₀→N₁[0] {m} m∈N₀ f[suc]≡f[0] =
    N₁.suc≢0 (val∈ran N₀→N₁-fun m∈N₀) $ sym $
    begin N₁.zero ≡⟨ sym f[0] ⟩
          N₀→N₁ [ zero ] ≡⟨ sym f[suc]≡f[0] ⟩
          N₀→N₁ [ suc m ] ≡⟨ f[suc] m∈N₀ ⟩
          N₁.suc (N₀→N₁ [ m ]) ∎

N₀→N₁-surj : surjective N₀→N₁ N₁.ℕ
N₀→N₁-surj = from (fun-surj⇔surj N₀→N₁-fun) $
  (⋁ m ∈ N₀.ℕ , N₀→N₁ [ m ] ≡ n) by-ℕ-induction-on n
  [base: N₀.zero , N₀.0∈ℕ , f[0]
  ,step: (λ { _ (n , n∈N₀ , refl) →
    N₀.suc n , N₀.suc∈ℕ n∈N₀ , f[suc] n∈N₀ }) ]
  where open N₁

open Bijection N₀→N₁-fun N₀→N₁-inj N₀→N₁-surj

N₀↔N₁ : bijection N₀→N₁-fun
N₀↔N₁ = bij
