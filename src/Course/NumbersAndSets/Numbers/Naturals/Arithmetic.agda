{-# OPTIONS --exact-split #-}
open import Course.NumbersAndSets.Numbers.Naturals.Axioms

module Course.NumbersAndSets.Numbers.Naturals.Arithmetic (nat : Peanoℕ) where

open import Course.NumbersAndSets.Base
open import Course.NumbersAndSets.Numbers.Naturals.Recursion nat
open import Course.NumbersAndSets.Numbers.Naturals.Literal nat
open import Course.NumbersAndSets.Operation
open Peanoℕ nat

infixl 31 _*_
infixl 30 _+_

module +Def (n : set) where
  open RecDef n (λ _ n+m → suc n+m) public

_+_ : (n m : set) → set
n + m = f [ m ]
  where open +Def n 

open ≡-Reasoning

module _ {n} where
  open +Def n

  +-0 : n + 0 ≡ n
  +-0 = f[0]

  +-suc : ⋀ m ∈ ℕ , n + suc m ≡ suc (n + m)
  +-suc = f[suc]

private variable n : set

suc-+ : ⋀ m ∈ ℕ , suc n + m ≡ suc (n + m)
suc-+ {n} = suc n + m ≡ suc (n + m) by-ℕ-induction-on m
  [base: begin suc n + 0 ≡⟨ +-0 ⟩
               suc n ≡⟨ cong suc $ sym +-0 ⟩
               suc (n + 0) ∎
  ,step: (λ {m} m∈ℕ IH → begin
    suc n + suc m ≡⟨ +-suc m∈ℕ ⟩
    suc (suc n + m) ≡⟨ cong suc IH ⟩
    suc (suc (n + m)) ≡⟨ cong suc $ sym $ +-suc m∈ℕ ⟩
    suc (n + suc m) ∎) ]

private module + = SingleOp ℕ _+_

0-+ : +.LeftIdentity 0
0-+ = 0 + n ≡ n by-ℕ-induction-on n
  [base: +-0
  ,step: (λ {n} n∈ℕ 0+n≡n → begin
    0 + suc n ≡⟨ +-suc n∈ℕ ⟩
    suc (0 + n) ≡⟨ cong suc 0+n≡n ⟩
    suc n ∎) ]

+-comm : +.Commutative
+-comm = (⋀ m ∈ ℕ , n + m ≡ m + n) by-ℕ-induction-on n
  [base: (λ {m} m∈ℕ → begin 0 + m ≡⟨ 0-+ m∈ℕ ⟩
                             m ≡⟨ sym +-0 ⟩
                             m + 0 ∎)
  ,step: (λ {n} n∈ℕ IH {m} m∈ℕ → begin
    suc n + m ≡⟨ suc-+ m∈ℕ ⟩
    suc (n + m) ≡⟨ cong suc $ IH m∈ℕ ⟩
    suc (m + n) ≡⟨ sym $ +-suc n∈ℕ ⟩
    m + suc n ∎) ]

+∈ℕ : +.Closed
+∈ℕ {n} n∈ℕ = n + m ∈ ℕ by-ℕ-induction-on m
  [base: subst (_∈ ℕ) (sym +-0) n∈ℕ
  ,step: (λ m∈ℕ m+n∈ℕ → subst (_∈ ℕ) (sym $ +-suc m∈ℕ) $ suc∈ℕ m+n∈ℕ)
  ]

+-assoc : ∀{n} → ⋀ m ∈ ℕ , ⋀ k ∈ ℕ , n + (m + k) ≡ n + m + k
+-assoc {n}{m} m∈ℕ = n + (m + k) ≡ n + m + k
  by-ℕ-induction-on k
  [base: begin n + (m + 0) ≡⟨ cong (n +_) +-0 ⟩
               n + m ≡⟨ sym +-0 ⟩
               n + m + 0 ∎
  ,step: (λ {k} k∈ℕ IH → begin
    n + (m + suc k) ≡⟨ cong (n +_) $ +-suc k∈ℕ ⟩
    n + suc (m + k) ≡⟨ +-suc $ +∈ℕ m∈ℕ k∈ℕ ⟩
    suc (n + (m + k)) ≡⟨ cong suc IH ⟩
    suc (n + m + k) ≡⟨ sym $ +-suc k∈ℕ ⟩
    n + m + suc k ∎) ]

+-cancelʳ : ⋀ n ∈ ℕ , ⋀ m ∈ ℕ , ⋀ k ∈ ℕ , (n + k ≡ m + k → n ≡ m)
+-cancelʳ {n} n∈ℕ {m} m∈ℕ = (n + k ≡ m + k → n ≡ m) by-ℕ-induction-on k
  [base: (λ n+0≡m+0 → begin
    n ≡⟨ sym +-0 ⟩
    n + 0 ≡⟨ n+0≡m+0 ⟩
    m + 0 ≡⟨ +-0 ⟩
    m ∎)
  ,step: (λ {k} k∈ℕ IH n+sk≡m+sk → IH $ S-inj (+∈ℕ n∈ℕ k∈ℕ)(+∈ℕ m∈ℕ k∈ℕ) $
    begin suc (n + k) ≡⟨ sym $ +-suc k∈ℕ ⟩
          n + suc k ≡⟨ n+sk≡m+sk ⟩
          m + suc k ≡⟨ +-suc k∈ℕ ⟩
          suc (m + k) ∎)
  ]

module *Def (n : set) where
  open RecDef 0 (λ _ n*m → n*m + n) public

_*_ : (n m : set) → set
n * m = f [ m ]
  where open *Def n

private module * = SingleOp ℕ _*_

module _ {n} where
  open *Def n

  *-0 : n * 0 ≡ 0
  *-0 = f[0]

  *-suc : ⋀ m ∈ ℕ , n * suc m ≡ n * m + n
  *-suc = f[suc]

0-* : *.LeftZero 0
0-* = 0 * n ≡ 0 by-ℕ-induction-on n
  [base: *-0
  ,step: (λ {n} n∈ℕ IH → begin
    0 * suc n ≡⟨ *-suc n∈ℕ ⟩
    0 * n + 0 ≡⟨ cong (_+ 0) IH ⟩
    0 + 0 ≡⟨ +-0 ⟩
    0 ∎)
  ]

suc-* : ⋀ n ∈ ℕ , ⋀ m ∈ ℕ , suc n * m ≡ n * m + m
suc-* {n} n∈ℕ = suc n * m ≡ n * m + m by-ℕ-induction-on m
  [base: begin suc n * 0 ≡⟨ *-0 ⟩
               0 ≡⟨ sym +-0 ⟩
               0 + 0 ≡⟨ cong (_+ 0) $ sym *-0 ⟩
               n * 0 + 0 ∎
  ,step: (λ {m} m∈ℕ IH → begin
    suc n * suc m ≡⟨ *-suc m∈ℕ ⟩
    suc n * m + suc n ≡⟨ cong (_+ suc n) IH ⟩
    n * m + m + suc n ≡⟨ sym $ +-assoc m∈ℕ (suc∈ℕ n∈ℕ) ⟩
    n * m + (m + suc n) ≡⟨ cong (n * m +_) $ +-suc n∈ℕ ⟩
    n * m + suc (m + n) ≡⟨ cong (n * m +_) $ sym $ suc-+ n∈ℕ ⟩
    n * m + (suc m + n) ≡⟨ cong (n * m +_) $ +-comm (suc∈ℕ m∈ℕ) n∈ℕ ⟩
    n * m + (n + suc m) ≡⟨ +-assoc n∈ℕ (suc∈ℕ m∈ℕ) ⟩
    n * m + n + suc m ≡⟨ cong (_+ suc m) $ sym $ *-suc m∈ℕ ⟩
    n * suc m + suc m ∎)
  ]

*∈ℕ : *.Closed
*∈ℕ {n} n∈ℕ = n * m ∈ ℕ by-ℕ-induction-on m
  [base: subst (_∈ ℕ) (sym *-0) 0∈ℕ
  ,step: (λ m∈ℕ m*n∈ℕ → subst (_∈ ℕ) (sym $ *-suc m∈ℕ) $ +∈ℕ m*n∈ℕ n∈ℕ)
  ]

open TwoOp ℕ

*-distribˡ-+ : _*_ DistributesOverˡ _+_
*-distribˡ-+ {n} n∈ℕ {m} m∈ℕ = n * (m + k) ≡ n * m + n * k
  by-ℕ-induction-on k
  [base: begin n * (m + 0) ≡⟨ cong (n *_) +-0 ⟩
               n * m ≡⟨ sym +-0 ⟩
               n * m + 0 ≡⟨ cong (n * m +_) $ sym *-0 ⟩
               n * m + n * 0 ∎
  ,step: (λ {k} k∈ℕ IH → begin
    n * (m + suc k) ≡⟨ cong (n *_) $ +-suc k∈ℕ ⟩
    n * suc (m + k) ≡⟨ *-suc $ +∈ℕ m∈ℕ k∈ℕ ⟩
    n * (m + k) + n ≡⟨ cong (_+ n) IH ⟩
    n * m + n * k + n ≡⟨ (sym $ +-assoc (*∈ℕ n∈ℕ k∈ℕ) n∈ℕ) ⟩
    n * m + (n * k + n) ≡⟨ cong (n * m +_) $ sym $ *-suc k∈ℕ ⟩
    n * m + n * suc k ∎)
  ]

*-comm : *.Commutative
*-comm = (⋀ m ∈ ℕ , n * m ≡ m * n) by-ℕ-induction-on n
  [base: (λ {m} m∈ℕ → begin 0 * m ≡⟨ 0-* m∈ℕ ⟩
                             0 ≡⟨ sym *-0 ⟩
                             m * 0 ∎)
  ,step: (λ {n} n∈ℕ IH {m} m∈ℕ → begin
    suc n * m ≡⟨ suc-* n∈ℕ m∈ℕ ⟩
    n * m + m ≡⟨ cong (_+ m) $ IH m∈ℕ ⟩
    m * n + m ≡⟨ sym $ *-suc n∈ℕ ⟩
    m * suc n ∎) ]

*-distribʳ-+ : _*_ DistributesOverʳ _+_
*-distribʳ-+ {n} n∈ℕ {m} m∈ℕ {k} k∈ℕ = begin
  (m + k) * n ≡⟨ *-comm (+∈ℕ m∈ℕ k∈ℕ) n∈ℕ ⟩
  n * (m + k) ≡⟨ *-distribˡ-+ n∈ℕ m∈ℕ k∈ℕ ⟩
  n * m + n * k ≡⟨ cong₂ _+_ (*-comm n∈ℕ m∈ℕ) (*-comm n∈ℕ k∈ℕ) ⟩
  m * n + k * n ∎

*-assoc : *.Associative
*-assoc {n} n∈ℕ {m} m∈ℕ = n * (m * k) ≡ n * m * k by-ℕ-induction-on k
  [base: begin n * (m * 0) ≡⟨ cong (n *_) *-0 ⟩
               n * 0 ≡⟨ *-0 ⟩
               0 ≡⟨ sym *-0 ⟩
               n * m * 0 ∎
  ,step: (λ {k} k∈ℕ IH → begin
    n * (m * suc k) ≡⟨ cong (n *_) $ *-suc k∈ℕ ⟩
    n * (m * k + m) ≡⟨ *-distribˡ-+ n∈ℕ (*∈ℕ m∈ℕ k∈ℕ) m∈ℕ ⟩
    n * (m * k) + n * m ≡⟨ cong (_+ n * m) IH ⟩
    n * m * k + n * m ≡⟨ sym $ *-suc k∈ℕ ⟩
    n * m * suc k ∎)
  ]

*-1 : *.RightIdentity 1
*-1 {n} n∈ℕ = begin
  n * 1 ≡⟨ *-suc 0∈ℕ ⟩
  n * 0 + n ≡⟨ cong (_+ n) *-0 ⟩
  0 + n ≡⟨ 0-+ n∈ℕ ⟩
  n ∎  

1-* : *.LeftIdentity 1
1-* = 1 * n ≡ n by-ℕ-induction-on n
  [base: *-0
  ,step: (λ {n} n∈ℕ IH → begin
    1 * suc n ≡⟨ *-suc n∈ℕ ⟩
    1 * n + 1 ≡⟨ cong (_+ 1) IH ⟩
    n + 1 ≡⟨ +-suc 0∈ℕ ⟩
    suc (n + 0) ≡⟨ cong suc +-0 ⟩
    suc n ∎ )
  ]
