{-# OPTIONS --exact-split #-}
module Course.NumbersAndSets.Sets where

open import ZF.Foundation renaming (_⇔_ to _↔_) hiding (_∨_) public
open import Course.NumbersAndSets.Logic

module SetVars where
  variable A B C X Y Z x y z : set
open SetVars

infixr 22 _Δ_
_Δ_ : (A B : set) → set
A Δ B = (A - B) ∪ (B - A)

open import Function.Equivalence.Reasoning

∈Δ : x ∈ A Δ B ↔ x ∈ A xor x ∈ B
∈Δ {x} {A} {B} = begin
  x ∈ A Δ B ⇔⟨ {!!} ⟩
  ((x ∈ A ∧ x ∉ B) ∨ ?) ⇔⟨ ? ⟩
  x ∈ A xor x ∈ B ∎
