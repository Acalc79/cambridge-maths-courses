{-# OPTIONS --exact-split #-}
open import Course.NumbersAndSets.Base

module Course.NumbersAndSets.Quotient where

open import Course.NumbersAndSets.Relation as Rel hiding (module Notation)
open import Course.NumbersAndSets.Operation as Op hiding (module Notation)

infixl 25 _/_
_/_ : (A R : set) → set
A / ~ = ｛ ⟦ x ⟧ ∣ x ∈ A ｝
  where open RelDefs ~ A

open RelVars hiding (Q)

open import Function.Equivalence.Reasoning

abstract
  ∈-/- : ∀{A R' x} → let open RelDefs R' A
    in --------------------------------------------------
    x ∈ A / R' ⇔ (⋁ a ∈ A , ⟦ a ⟧ ≡ x)
  ∈-/- {A}{R'}{x} = ∈fun-rep ⟦_⟧
    where open RelDefs R' A

open SetVars

module Operation (A R op : set) where
  open RelDefs R A renaming (_~_ to infix 19 _~_)
  Q = A / R
  open Op.Notation op

  quot-op : set
  quot-op = reify (Q × Q) Q
    λ x y → ∃₂ λ x₀ x₁ → x ≡ ⟨ x₀ ، x₁ ⟩ ∧
            ∃₂ λ a b → x₀ ≡ ⟦ a ⟧ ∧ x₁ ≡ ⟦ b ⟧ ∧
            ∃ λ c → y ≡ ⟦ c ⟧ ∧ [ a ∙ b ]= c

  to-⟨,⟩∈quot-op : ⋀ a ∈ A , ⋀ b ∈ A , ⋀ c ∈ A ,
    ([ a ∙ b ]= c → ⟨ ⟨ ⟦ a ⟧ ، ⟦ b ⟧ ⟩ ، ⟦ c ⟧ ⟩ ∈ quot-op)
  to-⟨,⟩∈quot-op {a} a∈A {b} b∈A {c} c∈A a∙b=c = to ⟨,⟩∈reify $
      to ⟨,⟩∈× (to ∈-/- (a , a∈A , refl) , to ∈-/- (b , b∈A , refl)) ,
      to ∈-/- (c , c∈A , refl) ,
      ⟦ a ⟧ , ⟦ b ⟧ , refl ,
      a , b , refl , refl ,
      c , refl ,
      a∙b=c
  
  module _ (op-is-op : op is-op-on A)
           (eq : equivalence)
           (preserv : op preserves R) where
    op-quot-op : quot-op is-op-on Q
    op-quot-op = ∶⇀-⁻¹surj→∶⟶ pfun-quot-op λ x∈Q×Q → case from ∈× x∈Q×Q of λ
      { (x₀ , x₀∈Q , x₁ , x₁∈Q , refl) → case from ∈-/- x₀∈Q , from ∈-/- x₁∈Q of λ
      { ((a , a∈A , refl) , b , b∈A , refl) →
        ⟦ a ∙ b ⟧ ,
        let a∙b∈A : a ∙ b ∈ A
            a∙b∈A = val∈ran op-is-op $ to ⟨,⟩∈× $ a∈A , b∈A
        in to ∈⁻¹ (
          ⟦ a ∙ b ⟧ , ⟨ ⟦ a ⟧ ، ⟦ b ⟧ ⟩ , refl ,
          to-⟨,⟩∈quot-op a∈A b∈A a∙b∈A (
            from (op-is-op [ ⟨ a ، b ⟩ ]≡) $ to ⟨,⟩∈× (a∈A , b∈A) , refl))}}
      where
      pfun-quot-op : quot-op ∶ Q × Q ⇀ Q
      pfun-quot-op = rel-⁻¹inj→∶⇀ reify-rel λ {x}{x'}{y} xy∈q-op⁻¹ x'y∈q-op⁻¹ →
        case from ⟨,⟩∈reify (from ⟨,⟩∈⁻¹ xy∈q-op⁻¹) ,
             from ⟨,⟩∈reify (from ⟨,⟩∈⁻¹ x'y∈q-op⁻¹) of λ
        { ((y∈Q×Q , x∈Q , _ , _ , refl ,
            a , b , refl , refl , c , refl , a∙b≡c) ,
           (_ , x'∈Q , _ , _ , [a][b]≡[a'][b'] ,
            a' , b' , refl , refl , c' , refl , a'∙b'≡c')) →
        case from ⟨,⟩≡ [a][b]≡[a'][b'] of λ { ([a]≡[a'] , [b]≡[b']) →
        let c'∈A = ran⊆ (fun→rel op-is-op) $ to ∈ran $ ⟨ a' ، b' ⟩ , a'∙b'≡c'
            a'b'∈A×A = dom⊆ (fun→rel op-is-op) $ to ∈dom $ c' , a'∙b'≡c'
            a'∈A = proj₁ $ from ⟨,⟩∈× a'b'∈A×A
            b'∈A = proj₂ $ from ⟨,⟩∈× a'b'∈A×A
            a~a' = from (⟦⟧≡ eq a'∈A) [a]≡[a']
            b~b' = from (⟦⟧≡ eq b'∈A) [b]≡[b']
        in to (⟦⟧≡ eq c'∈A) $ preserv a~a' b~b' a∙b≡c a'∙b'≡c'}}
