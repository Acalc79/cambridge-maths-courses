{-# OPTIONS --without-K --exact-split --safe #-}
module Course.DiscreteMaths.Proofs where

open import Level
open import Relation.Binary.PropositionalEquality public
open import Data.Product public

module LevelVars where
  variable ℓ ℓ₀ ℓ₁ ℓ₂ l l₀ l₁ l₂ : Level

open LevelVars

∃!-syntax : {A : Set ℓ₀} → (A → Set ℓ₁) → Set (ℓ₀ ⊔ ℓ₁)
∃!-syntax = ∃! _≡_

syntax ∃!-syntax (λ x → B) = ∃![ x ] B

infixr 1 _⇒_
_⇒_ : Set ℓ₀ → Set ℓ₁ → Set _
P ⇒ Q = P → Q

module PropositionalVars where
  variable P Q R S T P' Q' R' P₀ P₁ P₂ R₀ R₁ R₂ Q₀ Q₁ Q₂ : Set ℓ

open PropositionalVars

open import Data.Empty public renaming (⊥ to False)

infixr 4 ¬_
¬_ : Set ℓ → Set _
¬_ = _⇒ False

