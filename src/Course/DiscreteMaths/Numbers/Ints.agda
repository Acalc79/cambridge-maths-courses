{-# OPTIONS --without-K --exact-split --safe #-}
module Course.DiscreteMaths.Numbers.Ints where

open import Course.DiscreteMaths.Proofs

open import Data.Integer public
open import Data.Integer.Properties public
open import Data.Integer.Solver
open +-*-Solver public
  using (con; _:=_; _:*_; _:+_; _:-_; Polynomial; solve)

pattern 0ᶻ = + 0
pattern 1ᶻ = + 1
pattern 2ᶻ = + 2
pattern 3ᶻ = + 3
pattern -1ᶻ = -[1+ 0 ]
pattern -2ᶻ = -[1+ 1 ]
pattern -3ᶻ = -[1+ 2 ]

module ℤVars where
  variable x y z x' y' z' x″ y″ z″ : ℤ

open import Function
open import Algebra.Definitions (_≡_ {A = ℤ})
open import Algebra.Consequences.Propositional

open ℤVars
open ≡-Reasoning

+-cancelˡ-≡ : LeftCancellative _+_
+-cancelˡ-≡ z {x}{y} p = begin
  x ≡⟨ p' x z ⟩
  (z + x) - z ≡⟨ cong (λ k → k - z) p ⟩
  (z + y) - z ≡⟨ sym $ p' y z ⟩
  y
  ∎
  where p' : ∀ x y → x ≡ (y + x) - y
        p' = solve 2 (λ x y → x := (y :+ x) :- y) refl

+-cancelʳ-≡ : RightCancellative _+_
+-cancelʳ-≡ = comm+cancelˡ⇒cancelʳ +-comm +-cancelˡ-≡
