{-# OPTIONS --without-K --exact-split --safe #-}
open import Axiom.DoubleNegationElimination
open import Course.DiscreteMaths.Proofs
open LevelVars
open PropositionalVars

module Course.DiscreteMaths.Proofs.Classical
  (dne : ∀{ℓ} → DoubleNegationElimination ℓ) where

by-contrapositive : (¬ Q ⇒ ¬ P) → P ⇒ Q
by-contrapositive contra p = dne λ ¬q → contra ¬q p

