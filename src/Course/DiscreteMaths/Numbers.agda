{-# OPTIONS --without-K --exact-split --safe #-}
module Course.DiscreteMaths.Numbers where

open import Course.DiscreteMaths.Proofs
open import Course.DiscreteMaths.Numbers.Ints
open ℤVars

-- Definition 7
odd : ℤ → Set
odd x = ∃[ i ] (2ᶻ * i + 1ᶻ ≡ x)

open import Function.Reasoning

odd! : odd x → ∃![ i ] (x ≡ 2ᶻ * i + 1ᶻ)
odd! (i , refl) = i , refl , λ {j}(p : 2ᶻ * i + 1ᶻ ≡ 2ᶻ * j + 1ᶻ) →
     +-cancelʳ-≡ (2ᶻ * i)(2ᶻ * j) p ∶ 2ᶻ * i ≡ 2ᶻ * j
  |> *-cancelˡ-≡ 2ᶻ i j (λ ()) ∶ i ≡ j

-- Proposition 8

*-odd : odd x → odd y → odd (x * y)
*-odd (i , refl) (j , refl) = k , solve 2 poly refl i j
  where k = 2ᶻ * i * j + i + j
        poly : ∀{n} → let p = Polynomial n in p → p → p × p
        poly i j = con 2ᶻ :* (con 2ᶻ :* i :* j :+ i :+ j) :+ con 1ᶻ
               := (con 2ᶻ :* i :+ con 1ᶻ) :* (con 2ᶻ :* j :+ con 1ᶻ)

-- Definition 9
