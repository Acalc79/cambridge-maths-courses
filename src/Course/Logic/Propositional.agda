{-# OPTIONS --without-K --exact-split --safe #-}
module Course.Logic.Propositional where

open import Relation.Binary.PropositionalEquality hiding ([_])
open import Relation.Nullary hiding (¬_)
open import Function hiding (_↔_)
open import Data.String as S hiding (_++_)
open import Data.List as L hiding (and; or)
open import Data.List.Membership.Propositional
open import Data.List.Membership.Propositional.Properties
open import Data.List.Relation.Binary.Subset.Propositional
open import Data.List.Relation.Unary.Any
open import Data.Bool as B hiding (_∧_; _∨_)

Symbol = String

data Op2 : Set where
  and or imply iff : Op2

infix 30 _↔_
infixr 30 _⟶_
infixr 31 _∨_
infixr 32 _∧_
infix 33 ¬_
data Formula : Set where
  Sym : (x : Symbol) → Formula
  ¬_ : (P : Formula) → Formula
  _`_`_ : (P : Formula)(op : Op2)(Q : Formula) → Formula

pattern _∧_ P Q = P ` and ` Q
pattern _∨_ P Q = P ` or ` Q
pattern _⟶_ P Q = P ` imply ` Q
pattern _↔_ P Q = P ` iff ` Q

symbols : Formula → List Symbol
symbols (Sym P) = [ P ]
symbols (¬ P) = symbols P
symbols (P ` _ ` Q) = symbols P ++ symbols Q

allsymbols : List Formula → List Symbol
allsymbols = concatMap symbols

Interpretation : Formula → Set
Interpretation F = (P : Symbol) → .(P ∈ symbols F) → Bool

restrict : (F F' : Formula) → .(symbols F' ⊆ symbols F) →
  (I : Interpretation F) → Interpretation F'
restrict F F' F'⊆F I P P∈F' = I P (F'⊆F P∈F')

val-op : (op : Op2)(b₁ b₂ : Bool) → Bool
val-op and = B._∧_
val-op or = B._∨_
val-op imply b₁ b₂ = not b₁ B.∨ b₂
val-op iff b₁ b₂ = does (b₁ B.≟ b₂)

interpret : (F : Formula)(I : Interpretation F) → Bool
interpret (Sym x) I = I x (here refl)
interpret (¬ P) I = not $ interpret P I
interpret (P ` op ` Q) I =
  val-op op 
  (interpret P $ restrict (P ∧ Q) P ∈-++⁺ˡ I)
  (interpret Q $ restrict (P ∧ Q) Q (∈-++⁺ʳ $ symbols P) I)

satisfies : (F : Formula)(I : Interpretation F) → Set
satisfies F I = interpret F I ≡ true

valid : (S : List Formula)(I : Interpretation)

removeImplyIff : (F : Formula) → Formula
removeImplyIff (Sym x) = Sym x
removeImplyIff (¬ P) = ¬ removeImplyIff P
removeImplyIff (P ∧ Q) = removeImplyIff P ∧ removeImplyIff Q
removeImplyIff (P ∨ Q) = removeImplyIff P ∨ removeImplyIff Q
removeImplyIff (P ⟶ Q) = ¬ removeImplyIff P ∨ removeImplyIff Q
removeImplyIff (P ↔ Q) = let P' = removeImplyIff P
                             Q' = removeImplyIff Q
                         in (¬ P' ∨ Q') ∧ (¬ Q' ∨ P')

pushNegIn : (F : Formula) → Formula
pushNegIn P@(Sym x) = P
pushNegIn P@(¬ Sym x) = P
pushNegIn (P ` op ` Q) = pushNegIn P ` op ` pushNegIn Q
pushNegIn (¬ (¬ P)) = pushNegIn P
pushNegIn (¬ (P ∧ Q)) = pushNegIn (¬ P) ∨ pushNegIn (¬ Q)
pushNegIn (¬ (P ∨ Q)) = pushNegIn (¬ P) ∧ pushNegIn (¬ Q)
pushNegIn (¬ (P ⟶ Q)) = ¬ pushNegIn (P ⟶ Q)
pushNegIn (¬ (P ↔ Q)) = ¬ pushNegIn (P ↔ Q)

toNNF : (F : Formula) → Formula
toNNF = pushNegIn ∘ removeImplyIff
